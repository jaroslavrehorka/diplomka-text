//
// Created by jrehorka on 4/1/17.
//

#ifndef LIDAR_ANALYZER_SOCKET_CLIENT_H
#define LIDAR_ANALYZER_SOCKET_CLIENT_H

#include <string>
#include <inetclientstream.hpp>
#include <memory>

namespace lidar_analyzer {

/// This structure represents connected client,
/// it hold its remote port and address.
///
struct SocketClient {
  std::unique_ptr<libsocket::inet_stream> remote_stream;

  const string &GetRemoteAddress() const {
	  return this->remote_stream->gethost();
  }
  const string &GetRemotePort() const {
	  return this->remote_stream->getport();
  }

  friend bool operator==(const SocketClient &p1, const SocketClient &p2) {
	  return
		  (p1.GetRemotePort() == p2.GetRemotePort()) &&
			  (p1.GetRemoteAddress() == p2.GetRemoteAddress());
  }
};
}

#endif //LIDAR_ANALYZER_SOCKET_CLIENT_H
