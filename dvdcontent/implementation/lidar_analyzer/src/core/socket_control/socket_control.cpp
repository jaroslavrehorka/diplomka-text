//
// Created by jrehorka on 3/18/17.
//

#include "model/options.h"
#include "socket_control.h"
#include "shared/model/line.h"

#include <algorithm>
#include <select.hpp>

lidar_analyzer::SocketControl::SocketControl(lidar_analyzer::Options &options)
	: options(options),
	  inet_client(libsocket::inet_stream()),
	  host_server(libsocket::inet_stream_server(options.sockets_host_address,
	                                            options.sockets_host_port,
	                                            LIBSOCKET_IPv4)) {

	this->clients_connection_thread = std::thread([=] { this->WaitForRemoteConnections(); });
}

lidar_analyzer::SocketControl::~SocketControl() {
	this->host_server.destroy();
	this->inet_client << "disconnect";
	this->inet_client.shutdown();
	this->inet_client.destroy();
}

void lidar_analyzer::SocketControl::Connect() {
	if (!this->options.quiet) {
		std::cout << "Connecting to " << this->options.sockets_server_address << ":"
		          << this->options.sockets_server_port << std::endl;
		std::cout.flush();
	}

	this->inet_client.connect(this->options.sockets_server_address,
	                          this->options.sockets_server_port,
	                          LIBSOCKET_IPv4);

	std::cout << "Successfully connected!" << std::endl;
}

void lidar_analyzer::SocketControl::WaitForRemoteConnections() {
	libsocket::selectset<libsocket::inet_stream_server> set1;
	set1.add_fd(this->host_server, LIBSOCKET_READ);

	lidar_analyzer::SocketClient new_client;
	libsocket::selectset<libsocket::inet_stream_server>::ready_socks readypair;

	while (true) {
		readypair = set1.wait(); // Wait for a connection and save the pair to the var

		if (readypair.first.size() == 0) {
			continue;
		}

		auto ready_srv =
			dynamic_cast<libsocket::inet_stream_server *>(readypair.first.back());
		readypair.first.pop_back();

		auto cl1 = ready_srv->accept2();

		new_client.remote_stream = std::move(cl1);

		std::cout << "Pripojen klient " << new_client.GetRemoteAddress() << ":" << new_client.GetRemotePort();
		this->connected_clients.push_back(std::move(new_client));
	}
}

void lidar_analyzer::SocketControl::SendData(const std::string &data) {
	for (const lidar_analyzer::SocketClient &client : this->connected_clients) {
		if (client.remote_stream == nullptr) {
			continue;
		}

		try {
			(*client.remote_stream).snd(data.c_str(), data.length());
		} catch (libsocket::socket_exception &ex) {
			throw std::runtime_error(ex.mesg);
		}
	}
}

void lidar_analyzer::SocketControl::SendData(const lidar_shared::Point &data) {
	this->SendData(
		std::to_string(data.quality) + "," +
			std::to_string(data.distance) + "," +
			std::to_string(data.angle)
	);
}

void lidar_analyzer::SocketControl::SendData(const std::vector<lidar_shared::Point> &data) {
	std::stringstream data_to_send;

	for (auto point : data) {
		data_to_send << point.Serialize();
	}

	this->SendData(data_to_send.str());
}

void lidar_analyzer::SocketControl::SendData(const std::vector<lidar_shared::Point> &point_data,
                                             const std::vector<lidar_shared::Line> &line_data) {
	std::stringstream data_to_send;

	for (auto point : point_data) {
		data_to_send << point.Serialize();
	}

	for (auto line : line_data) {
		data_to_send << line.Serialize();
	}

	this->SendData(data_to_send.str());
}

void lidar_analyzer::SocketControl::ReadData(std::vector<lidar_shared::Point> *points) {
	this->inet_client.rcv(this->buffer, this->kDataBufferSize);

	std::stringstream scan_data(this->buffer);
	std::string token;
	std::smatch base_match;

	points->clear();
	auto point_pattern = lidar_shared::Point::GetPattern();

	while (getline(scan_data, token, this->kPointDelimiter)) {
		if (std::regex_match(token, point_pattern)) {
			points->push_back(this->ParsePointString(token));
		}
	}
}

lidar_shared::Point lidar_analyzer::SocketControl::ParsePointString(const std::string &point_string) {
	lidar_shared::Point parsedPoint;
	const int kMessagePartsCount = 3;

	std::istringstream is(point_string);
	std::string parts[kMessagePartsCount];
	std::string token;

	int i = 0;
	while (getline(is, token, this->kPointAttributesDelimiter)) {
		parts[i] = token;
		++i;
	}

	if (i == kMessagePartsCount) {
		parsedPoint.angle = (float) ::atof(parts[2].c_str());
		parsedPoint.distance = ((float) ::atof(parts[1].c_str()));
		parsedPoint.quality = (float) ::atof(parts[0].c_str());
	} else {
		parsedPoint.ClearPoint();
	}

	return parsedPoint;
}
