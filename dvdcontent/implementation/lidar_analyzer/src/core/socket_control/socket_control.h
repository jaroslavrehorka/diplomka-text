//
// Created by jrehorka on 3/18/17.
//

#ifndef LIDAR_ANALYZER_SOCKET_CONTROL_H
#define LIDAR_ANALYZER_SOCKET_CONTROL_H

#include "model/options.h"
#include "socket_client.h"
#include "shared/model/line.h"
#include "shared/model/point.h"

#include <string>

#include <inetserverdgram.hpp>
#include <vector>
#include <thread>
#include <inetserverstream.hpp>

namespace lidar_analyzer {

/// This class is a simplified wrapper of libsocket library
/// that is used for interprocess communication.
class SocketControl {
private:
  const static char kPointDelimiter = ';';
  const static char kPointAttributesDelimiter = ',';
  const static uint32_t kDataBufferSize = 60000;
  char buffer[kDataBufferSize];
  lidar_analyzer::Options &options;
  std::thread clients_connection_thread;

  libsocket::inet_stream_server host_server;
  libsocket::inet_stream inet_client;
  std::vector<lidar_analyzer::SocketClient> connected_clients;

  lidar_shared::Point ParsePointString(const std::string &point_string);
public:
  /// Initializes SocketControl with given address and port.
  ///
  /// \param options program options with all settings neccessary to run
  SocketControl(lidar_analyzer::Options &options);
  ~SocketControl();

  /// Blocking method that waits for an incoming connection,
  /// block is released when a new connection is estabilished.
  void Connect();

  /// Blocking method that waits for an incoming connection,
  /// block is released when a new connection is estabilished.
  void WaitForRemoteConnections();

  /// Parses incomming lidar data string.
  ///
  /// Data string has following format:
  ///   quality;distance;angle;
  /// \return Parsed point in instance of lidar_shared::Point
  void ReadData(std::vector<lidar_shared::Point> *points);

  /// Sends data to remotely connected client.
  ///
  /// \param data string data to be send.
  void SendData(const string &data);

  /// Sends data to remotely connected client.
  ///
  /// \see SendData(string).
  void SendData(const lidar_shared::Point &data);

  /// Sends data to remotely connected client.
  ///
  /// \see SendData(string).
  void SendData(const std::vector<lidar_shared::Point> &data);

  /// Sends data to remotely connected client.
  ///
  /// \see SendData(string).
  void SendData(const std::vector<lidar_shared::Point> &point_data,
                const std::vector<lidar_shared::Line> &line_data);
};
}
#endif //LIDAR_ANALYZER_SOCKET_CONTROL_H
