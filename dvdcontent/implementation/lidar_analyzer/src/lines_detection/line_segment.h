//
// Created by jrehorka on 6.4.17.
//

#ifndef LIDAR_ANALYZER_LINE_SEGMENT_H
#define LIDAR_ANALYZER_LINE_SEGMENT_H

#include "shared/model/point.h"
#include "shared/model/point.h"

#include <list>

namespace lidar_analyzer {

///
/// This struct is a data container for holding string of points
/// segment with its significance.
///
/// Significance is a ratio between length of a line that goes through
/// first and last point and max(distance of all points from the line).
///
struct LineSegment {

  ///
  /// Ratio between length of a line and maximal distance.
  ///
  double significance;

  ///
  /// Flag that indicates that this Line segment is empty.
  ///
  bool empty;

  ///
  /// List of points that belongs to this segment.
  ///
  std::list<lidar_shared::Point> line_segment;
};
}

#endif //LIDAR_ANALYZER_LINE_SEGMENT_H
