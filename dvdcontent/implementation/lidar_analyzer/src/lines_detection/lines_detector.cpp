//
// Created by jrehorka on 4/4/17.
//

#include "lines_detector.h"
#include "points_linker.h"
#include "lines_segmentor.h"

#include <iostream>
#include <list>

void lidar_analyzer::LinesDetector::AnalyzeData(std::vector <lidar_shared::Point> *scan_data_out,
                                                std::vector <lidar_shared::Line> *lines_data_out) {

	lines_data_out->clear();

	auto linker = lidar_analyzer::PointsLinker(*scan_data_out);
	auto out_data = std::vector < std::list < lidar_shared::Point >> ();

	linker.FindStrings(&out_data);
	scan_data_out->clear();

	auto strings_segmentor = lidar_analyzer::LinesSegmentor();
	strings_segmentor.FindSubStrings(&out_data);

	this->DetectLines(out_data, lines_data_out);

	std::cout.flush();

	for (const auto &string: out_data) {
		for (const auto &point: string) {
			scan_data_out->push_back(point);
		}
	}
}

void lidar_analyzer::LinesDetector::DetectLines(std::vector <std::list<lidar_shared::Point>> &point_strings,
                                                std::vector <lidar_shared::Line> *lines_data_out) {

	if (lines_data_out == nullptr) {
		std::cerr << "lines_data_out must not be null" << std::endl;
		return;
	}

	double deviance_sum;
	int sample_size;
	auto current_line = lidar_shared::Line();
	bool is_current_line_valid;

	for (auto &string: point_strings) {
		deviance_sum = 0;
		sample_size = 0;
		is_current_line_valid = false;

		for (auto it = string.begin(); it != string.end(); ++it) {

			deviance_sum += it->direction_change_angle;
			++sample_size;

			if (it->direction_change_angle <= this->kMaximalDeviance &&
				sample_size >= this->kMinimalLineSize && (*it) == string.back()) {
				is_current_line_valid = true;
			}

			if (it->direction_change_angle > this->kMaximalDeviance) {
				deviance_sum = 0;
				sample_size = 0;
			}
		}

		if (is_current_line_valid) {
			current_line = lidar_shared::Line::SimpleRegression(string);
			current_line.rmse = current_line.CalculateRmse(string);

			lines_data_out->push_back(current_line);
		}
	}
}
