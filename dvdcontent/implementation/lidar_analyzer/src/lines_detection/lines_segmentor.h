//
// Created by jrehorka on 4/5/17.
//

#ifndef LIDAR_ANALYZER_LINES_SEGMENTOR_H
#define LIDAR_ANALYZER_LINES_SEGMENTOR_H

#include "shared/model/point.h"
#include "line_segment.h"

#include <list>
#include <vector>

namespace lidar_analyzer {
class LinesSegmentor {
private:
  constexpr static double kMaxDifferenceAngle = 10;
  constexpr static double kMaxLineDistanceTreshold = 10;
  constexpr static double kMinimalSegmentSize = 8;

  std::vector<std::list<lidar_shared::Point>> list_of_strings;

  ///
  /// Calculates angle θ, between 3 points.
  ///
  /// \param point_a initial point of first vector.
  /// \param point_b initial point of first vector.
  /// \param peak terminal point of both vectors.
  /// \return angle between two vectors in radians.
  ///
  double GetAngleBetweenVectors(lidar_shared::Point &point_a,
                                lidar_shared::Point &point_b,
                                lidar_shared::Point &peak) const;

  std::list<lidar_analyzer::LineSegment> AnalyzeLineSegment(std::list<lidar_shared::Point> &single_segment);

public:
  /// Finds substrings in a list of strings.
  ///
  /// Every change in string create by connected objects is valued by angle change.
  /// If such change is significant the string is split in such point in two points,
  /// where the point is in both new substrings.
  ///
  /// \param list_of_strings_out
  void FindSubStrings(std::vector<std::list<lidar_shared::Point>> *list_of_strings_out);
};
}

#endif //LIDAR_ANALYZER_LINES_SEGMENTOR_H
