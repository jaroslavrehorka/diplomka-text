//
// Created by jrehorka on 4/3/17.
//

#include <iostream>
#include <list>
#include "points_linker.h"

void lidar_analyzer::PointsLinker::FindStrings(std::vector<std::list<
	lidar_shared::Point>> *list_of_strings_out) {

	if (list_of_strings_out == nullptr) {
		std::cerr << "list_of_strings_out must not be null" << std::endl;
		return;
	}

	list_of_strings_out->clear();
	this->SortPointsByAngle();

	auto init_point_index = this->GetFirstNonZeroPointIndex();
	auto string_vector = std::list<lidar_shared::Point>();
	int feature_point_index;
	int previous_feature_point_index;

	while (init_point_index != this->kInvalidPointIndex) {
		feature_point_index = init_point_index;

		while (feature_point_index != this->kInvalidPointIndex) {
			string_vector.push_back(this->scan_data[feature_point_index]);
			previous_feature_point_index = feature_point_index;
			feature_point_index = this->GetNextPoint(feature_point_index);
			this->scan_data[previous_feature_point_index].ClearPoint();
		}

		feature_point_index = this->GetNextPoint(init_point_index);
		if (feature_point_index != this->kInvalidPointIndex) {
			while (feature_point_index != this->kInvalidPointIndex) {
				string_vector.push_front(this->scan_data[feature_point_index]);
				this->scan_data[feature_point_index].ClearPoint();
				previous_feature_point_index = feature_point_index;
				feature_point_index = this->GetNextPoint(feature_point_index);
				this->scan_data[previous_feature_point_index].ClearPoint();
			}
		}

		list_of_strings_out->push_back(string_vector);
		string_vector.clear();
		init_point_index = this->GetFirstNonZeroPointIndex();
	}
}

int lidar_analyzer::PointsLinker::GetNextPoint(int feature_pixel_index, float distance_threshold) {
	if (feature_pixel_index < 0 || feature_pixel_index >= this->scan_data.size()) {
		return this->kInvalidPointIndex;
	}

	auto feature_pixel = this->scan_data[feature_pixel_index];

	auto left_neighbour_pixel_index = (feature_pixel_index - 1) % 360;
	auto left_neighbour = this->scan_data[left_neighbour_pixel_index];

	if (!left_neighbour.IsZeroPoint()
		&& std::abs(feature_pixel.distance - left_neighbour.distance) <= distance_threshold) {
		return left_neighbour_pixel_index;
	}

	auto right_neighbour_pixel_index = (feature_pixel_index + 1) % 360;
	auto right_neighbour = this->scan_data[right_neighbour_pixel_index];

	if (!right_neighbour.IsZeroPoint()
		&& std::abs(feature_pixel.distance - right_neighbour.distance) <= distance_threshold) {
		return right_neighbour_pixel_index;
	}

	return this->kInvalidPointIndex;
}

int lidar_analyzer::PointsLinker::GetFirstNonZeroPointIndex() {

	for (auto i = 0; i < this->scan_data.size(); i++) {
		if (!this->scan_data[i].IsZeroPoint()) {
			return i;
		}
	}

	return this->kInvalidPointIndex;
}



