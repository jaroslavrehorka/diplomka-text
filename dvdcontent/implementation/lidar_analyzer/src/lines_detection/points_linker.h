//
// Created by jrehorka on 4/3/17.
//

#ifndef LIDAR_ANALYZER_POINTSLINKER_H
#define LIDAR_ANALYZER_POINTSLINKER_H

#include "shared/model/point.h"

#include <vector>
#include <list>
#include <algorithm>

namespace lidar_analyzer {

/// This class handles linking procedure of edge detection.
///
/// Basically it creates strings of neighbouring points detected
/// by a lidar (one iteration).
class PointsLinker {
private:
  static const int kInvalidPointIndex = -1;
  std::vector<lidar_shared::Point> &scan_data;

  /// Sorts points in vector by angle in ascending data order.
  ///
  /// \param scan_data data to be sorted.
  void SortPointsByAngle() {
	  std::sort(this->scan_data.begin(), this->scan_data.end(),
	            [](const lidar_shared::Point first, const lidar_shared::Point second) {
		          return first.angle < second.angle;
	            }
	  );
  }

  /// This method finds neighbour of feature pixel.
  /// Thanks to one layered, polar-like 2D lidar scan, the pixel has
  /// maximally 2 neighbours.
  ///
  /// We just need to decide maximal difference in radial coordinate to define
  /// the threshold for neighbouring pixels.
  ///
  /// \param feature_pixel_index index of pixel in this->scan_data.
  /// \param distance_threshold maximal distance difference from origin between neighbouring two pixels (milimeters).
  /// \return index of neighbour pixel, if not found, our out of range -1 is returned.
  int GetNextPoint(int feature_pixel_index, float distance_threshold = 100);

  //// Iterates through this->scan_data and finds first non-zero point (point that has values
  /// distance, angle and quality > 0).
  ///
  /// \return first non-zero point index from beginning, if not found kInvalidPointIndex is returned.
  int GetFirstNonZeroPointIndex();
public:

  PointsLinker(std::vector<lidar_shared::Point> &scan_data) : scan_data(scan_data) {};

  /// This method creates array of strings that consist of "connected"
  /// points from LIDAR data.
  ///
  /// \param list_of_strings_out vector of strings to be filled.
  void FindStrings(std::vector<std::list<lidar_shared::Point>> *list_of_strings_out);
};
};

#endif //LIDAR_ANALYZER_POINTSLINKER_H
