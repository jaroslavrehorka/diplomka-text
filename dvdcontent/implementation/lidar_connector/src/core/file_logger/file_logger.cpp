//
// Created by jrehorka on 9.5.17.
//

#include "core/file_logger/las_logger/las_logger.h"
#include "core/file_logger/plain_text_logger/plain_text_logger.h"
#include "file_logger.h"

#include <memory>
#include <vector>
#include <shared/libs/extensions/string_extensions.h>

lidar_connector::FileLogger::FileLogger(std::string filename) {
	if (lidar_shared::StringExtensions::HasSuffix(filename, ".las")) {
		this->logger = std::make_unique<lidar_connector::LasLogger>(filename);
	} else {
		this->logger = std::make_unique<lidar_connector::PlainTextLogger>(filename);
	}
}

void lidar_connector::FileLogger::SaveIteration(const std::vector<lidar_shared::Point> scan_iteration) {
	this->logger->SaveIteration(scan_iteration);
}
