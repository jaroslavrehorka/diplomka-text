//
// Created by jrehorka on 9.5.17.
//

#ifndef LIDAR_CONNECTOR_FILE_LOGGER_H
#define LIDAR_CONNECTOR_FILE_LOGGER_H

#include "Ifile_logger.h"
#include <shared/model/point.h>

#include <string>
#include <memory>

namespace lidar_connector {
class FileLogger {
private:
  std::unique_ptr<IFileLogger> logger;
public:
  FileLogger(std::string filename);
  void SaveIteration(const std::vector<lidar_shared::Point> scan_iteration);
};
}

#endif //LIDAR_CONNECTOR_FILE_LOGGER_H
