#include "rplidar.h"
#include "rp_lidar_driver.h"

#include <stdexcept>
#include <sstream>
#include <iostream>

lidar_connector::RplidarDriver::RplidarDriver() : is_scanning(false) {
	this->device_info = lidar_device_info();
	this->device_info.model_name_ = "RPLidar A2";
	this->device_info.manufacturer_ = "Slamtec";
	this->device_info.default_baudrate_ = 115200;
#ifdef _WIN32
	// use default com port
	this->device_info.default_port_ = "\\\\.\\com3";
#else
	this->device_info.default_port_ = "/dev/ttyUSB0";
#endif
}

lidar_connector::RplidarDriver::~RplidarDriver() {
	if (this->driver != nullptr) {
		this->driver->disconnect();
		rplidar_sdk::RPlidarDriver::DisposeDriver(this->driver);
	}
}

void lidar_connector::RplidarDriver::ParseInfoFromDevice() {
	rplidar_response_device_info_t info;
	auto result_info = this->driver->getDeviceInfo(info);
	if (IS_FAIL(result_info)) {
		throw std::runtime_error("Cannot get device info.");
	}

	std::ostringstream serial_number_ss;

	for (int pos = 0; pos < 16; ++pos) {
		serial_number_ss << std::uppercase << std::hex << (int) info.serialnum[pos];
	}

	this->device_info.serial_number_ = serial_number_ss.str();
	this->device_info.firmware_version_ = (info.firmware_version >> 8) + (info.firmware_version & 0xFF) / 100.0;
	this->device_info.hardware_version_ = info.hardware_version;
}

const bool lidar_connector::RplidarDriver::CheckLidarHealth() const {
	rplidar_response_device_health_t health_info;
	auto result_health = this->driver->getHealth(health_info);

	if (IS_OK(result_health)) {
		if (health_info.status == RPLIDAR_STATUS_ERROR) {
			throw std::runtime_error(
				std::string("Error, rplidar internal error detected. Please reboot the device to retry.\n")
					+ std::to_string(result_health));
		}

		return true;

	} else {
		throw std::runtime_error(std::string("Cannot retrieve lidar health code: ") + std::to_string(result_health));
	}
}

void lidar_connector::RplidarDriver::Connect() {
	this->driver = rplidar_sdk::RPlidarDriver::CreateDriver(rplidar_sdk::RPlidarDriver::DRIVER_TYPE_SERIALPORT);

	if (this->driver == nullptr) {
		throw std::runtime_error("Cannot create rplidar instance, insufficient memory.");
	}

	if (IS_FAIL(this->driver->connect(this->device_info.default_port_.c_str(), this->device_info.default_baudrate_))) {
		throw std::runtime_error(
			"Error, cannot bind to the specified serial port, please check permissions to: "
				+ this->device_info.default_port_);
	}

	this->ParseInfoFromDevice();
	this->CheckLidarHealth();
}

void lidar_connector::RplidarDriver::Reset() const {
	this->driver->reset();
}

const lidar_connector::lidar_device_info lidar_connector::RplidarDriver::GetDeviceInfo() const {
	return this->device_info;
}

const bool lidar_connector::RplidarDriver::IsConnected() const {
	return this->driver != nullptr && this->driver->isConnected();
}

void lidar_connector::RplidarDriver::StartScanning(uint16_t motor_pwd) {
	if (!this->IsConnected()) {
		throw std::runtime_error("The device is not connected.");
	}

	this->driver->startMotor();
	this->driver->startScan();
	this->driver->setMotorPWM(motor_pwd);
	this->is_scanning = true;
	this->past_scans = std::deque<std::vector<lidar_shared::Point>>();
}

void lidar_connector::RplidarDriver::StopScanning() {
	if (!this->IsConnected()) {
		std::cerr << "The device is not connected." << std::endl;
		return;
	}

	this->driver->stop();
	this->driver->stopMotor();
	this->is_scanning = false;
}

std::vector<lidar_shared::Point> lidar_connector::RplidarDriver::GetScanData(int iterations_to_average) {
	if (!this->IsConnected()) {
		throw std::runtime_error("The device is not connected.");
	}

	if (!this->is_scanning) {
		throw std::runtime_error("The device is not in scanning mode.");
	}

	auto points_vector = std::vector<lidar_shared::Point>(kScannedAngle);

	rplidar_response_measurement_node_t nodes[kScannedAngle];
	size_t count = kScannedAngle;

	auto op_result = this->driver->grabScanData(nodes, count);

	if (IS_OK(op_result)) {
		this->driver->ascendScanData(nodes, count);

		for (int pos = 0; pos < (int) count; ++pos) {
			points_vector[pos].angle =
				std::fmod(360 + 90 - (nodes[pos].angle_q6_checkbit >> RPLIDAR_RESP_MEASUREMENT_ANGLE_SHIFT)
					/ 64.0f, 360); // 360 - because rpilidar uses counterclockwise angle direction
			points_vector[pos].distance = nodes[pos].distance_q2 / 4.0f;
			points_vector[pos].quality = nodes[pos].sync_quality >> RPLIDAR_RESP_MEASUREMENT_QUALITY_SHIFT;
		}
	}

	if (iterations_to_average < 2) {
		return std::move(points_vector);
	}

	this->past_scans.push_back(points_vector);

	if (this->past_scans.size() <= iterations_to_average) {
		this->GetScanData(iterations_to_average);
		return points_vector;
	}

	this->past_scans.pop_front();
	return std::move(this->GetAverageScan());
}
std::vector<lidar_shared::Point> lidar_connector::RplidarDriver::GetAverageScan() const {
	auto average_scan = std::vector<lidar_shared::Point>();

	for (int i = 0; i < kScannedAngle; ++i) {
		auto point = lidar_shared::Point();
		int points_used = 0;
		point.ClearPoint();

		for (auto vect : this->past_scans) {
			if (vect[i].quality == 0) {
				continue;
			}

			point.quality += vect[i].quality;
			point.distance += vect[i].distance;
			point.angle += vect[i].angle;
			++points_used;
		}

		if (points_used == 0) {
			++points_used;
		}

		point.quality /= points_used;
		point.distance /= points_used;
		point.angle /= points_used;
		average_scan.push_back(point);
	}

	return std::move(average_scan);
}
