#include "core/bootstrap.h"
#include "core/socket_control/socket_control.h"
#include "driver/idriver.h"
#include "driver/rp_lidar_driver.h"
#include "model/options.h"
#include "shared/libs/extensions/string_extensions.h"

#include <iostream>
#include <csignal>
#include <getopt.h>
#include <memory>

int parse_args(int argc, char *argv[], lidar_connector::Options *options) {
	int c;
	const char *short_opt = "rhsvqd:p:a:l:i:m:";
	struct option long_opt[] =
		{
			{"help", no_argument, NULL, 'h'},
			{"single-run", no_argument, NULL, 's'},
			{"verbose", no_argument, NULL, 'v'},
			{"quiet", no_argument, NULL, 'q'},
			{"device", required_argument, NULL, 'd'},
			{"port", required_argument, NULL, 'p'},
			{"address", required_argument, NULL, 'a'},
			{"log", required_argument, NULL, 'l'},
			{"input", required_argument, NULL, 'i'},
			{"avg", required_argument, NULL, 'm'},
			{NULL, 0, NULL, 0}
		};

	while ((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1) {
		switch (c) {
		case -1:       /* no more arguments */
		case 0:        /* long options toggles */
			break;
		case 'a': options->sockets_host_address = optarg;
			break;
		case 'r': options->reset_device = true;
			break;
		case 'p': options->sockets_host_port = optarg;
			options->use_sockets = true;
			break;
		case 'd': options->lidar_port = optarg;
			break;
		case 'v': options->verbose = true;
			break;
		case 'q': options->quiet = true;
			break;
		case 's': options->single_scan = true;
			break;
		case 'i': options->read_from_file = true;
			options->input_file = optarg;
			break;
		case 'm': {
			auto parsed = std::atoi(optarg);
			options->iterations_to_average = parsed > 0 ? parsed : 1;
		}
			break;
		case 'l': options->log_to_file = true;
			options->log_file_name = optarg;
			if (lidar_shared::StringExtensions::HasSuffix(options->log_file_name, ".las")) {
				options->single_scan = true;
			}
			break;
		case 'h': std::cout << "Usage: " << argv[0] << " [OPTIONS]" << std::endl;
			std::cout
				<< "  -p <port>, --port=<port>        Port number that will be used as port for socket host"
				<< std::endl;
			std::cout
				<< "  -a <addr>, --address=<addr>     Address that will be used as an address for socket host,"
				<< std::endl
				<< "                                  if the port is not provided sockets won't be used, "
				<< std::endl
				<< "                                  if the address is not provided \"localhost\" will be used"
				<< std::endl;
			std::cout << "  -d <port>, --device=<port>      A lidar device port. Default is \"/dev/ttyUSB0\""
			          << std::endl;
			std::cout
				<< "  -l <file>, --log=<file>         Filename of log file. When has a \".las\" suffix, "
					"\n                                  the LAS file format is used, otherwise uses plain text."
					"\n                                  When saving to \".las\" file, the --single-run is forced."
				<< std::endl;
			std::cout << "  -v, --verbose                   Whether more verbose output should be used." << std::endl;
			std::cout
				<< "  -q, --quiet                     Whether quiet output mode should be used, has higher priority than verbose."
				<< std::endl;
			std::cout
				<< "  -s, --single-run                Flag that makes the LIDAR device to run only one scan cycle."
				<< std::endl;
			std::cout
				<< "  -i <file>, --input=<file>       Input file with a LIDAR data. The data is expected to be in format "
				<< std::endl
				<< "                                  - one line per each iteration" << std::endl
				<< "                                  - point by point in format \"<quality>,<distance>,<angle>;\""
					"\n                                   separated by a comma, ending with semicolon"
				<< std::endl;
			std::cout
				<< "  -m, --avg                       Number of iterations to be used for moving average (accepted values are > 0)"
				<< std::endl;
			std::cout << "  -h, --help                      Print this help and exit" << std::endl;
			std::cout << std::endl;
			return -1;
		default: std::cerr << argv[0] << ": invalid option -- " << c << std::endl;
			std::cerr << "Try `" << argv[0] << " --help' for more information." << std::endl;
			return 1;
		};
	};

	return 0;
}

int main(int argc, char *argv[]) {
	lidar_connector::Options options;
	auto parse_result = parse_args(argc, argv, &options);

	if (parse_result == 1) {
		return EXIT_FAILURE;
	} else if (parse_result == -1) {
		return EXIT_SUCCESS;
	}

	lidar_connector::Bootstrap bootstrap(options);
	return bootstrap.Run();
}