//
// Created by jrehorka on 4.5.17.
//

#ifndef LIDAR_SHARED_POINT_H
#define LIDAR_SHARED_POINT_H

#include "iprimitive.h"

#include <sstream>
#include <cmath>
#include <regex>

namespace lidar_shared {
struct Point : public IPrimitive {

  static const std::regex GetPattern() {
	  return std::regex("(\\d+\\.?\\d*),(\\d+\\.?\\d*),(\\d+\\.?\\d*)");
  }

  /// Quality of current measurement sample.
  float quality;

  /// Distance in mm.
  ///
  /// When equals 0 measurement is invalid.
  float distance;

  /// Polar coordinate system angle.
  /// 0 degrees at x axis, counterclockwise.
  float angle;

  /// We do not support 3d scans at the moment.
  int z = 0;

  double getAngleRads() const {
	  return this->angle * (M_PI / 180.0);
  }

  int getX() const { //todo: na double
	  return (int) (this->distance * std::cos(this->getAngleRads()));
  }

  int getY() const {
	  return (int) (this->distance * std::sin(this->getAngleRads()));
  }

  int getZ() const {
	  return this->z;
  }

  /// Size of direction change on this point in radians.
  /// If this poins is peak of straight angle change angle
  /// is == 0.
  double direction_change_angle = 0;

  /// Retuns string_change_angle in degrees.
  double GetStringChangeAngleDeg() const {
	  return direction_change_angle * (180 / M_PI);
  }

  /// \return Formated point info.
  const std::string GetString() const {
	  std::ostringstream os;

	  os << "Q: " << this->quality
	     << ", D: " << this->distance
	     << ", A: " << this->angle
	     << ", X: " << this->getX()
	     << ", Y: " << this->getY();

	  return os.str();
  }

  /// "Empty", invalid point.
  const bool IsZeroPoint() const {
	  return (this->distance == 0 ||
		  this->quality == 0);
  }

  void ClearPoint() {
	  this->angle = 0;
	  this->distance = 0;
	  this->quality = 0;
	  this->direction_change_angle = 0;
  }

  const std::string Serialize() const {
	  return std::to_string(this->quality) + "," +
		  std::to_string(this->distance) + "," +
		  std::to_string(this->angle) + ";";
  }

  bool operator==(const Point &other) {
	  return this->angle == other.angle &&
		  this->distance == other.distance;
  }

  bool operator!=(const Point &other) {
	  return !(*(this) == other);
  }

  static Point FromCartesian(float x, float y) {
	  auto point = lidar_shared::Point();

	  point.distance = std::sqrt(x * x + y * y);
	  point.angle = std::fmod(std::atan2(y, x) * (180.0f / (float) M_PI), 360.0f);
	  if (point.angle < 0) {
		  point.angle += 360;
	  }

	  return point;
  }

  /// Calculates Euclidean distance between two points.
  ///
  /// \param start_point
  /// \param end_point
  /// \return Euclidean distance between start_point and end_point
  static double GetDistanceBetweenPoints(const lidar_shared::Point &start_point,
                                         const lidar_shared::Point &end_point) {
	  auto a = std::pow(end_point.getY() - start_point.getY(), 2);
	  auto b = std::pow(end_point.getX() - start_point.getX(), 2);

	  return std::sqrt(
		  std::pow(end_point.getY() - start_point.getY(), 2) + std::pow(end_point.getX() - start_point.getX(), 2)
	  );
  }

  /// Calculates distance of point from line that is goes through points,
  /// from parameters.
  ///
  /// \param start_point starting point of the line.
  /// \param end_point ending point of the line.
  /// \param tested_point point that we want to know distance from the line.
  /// \return distance of tested_point from the line defined by start_point and end_point
  static double GetDistanceFromEndPointsLine(const lidar_shared::Point &start_point,
                                             const lidar_shared::Point &end_point,
                                             const lidar_shared::Point &tested_point) {

	  auto numerator = std::abs(
		  (end_point.getY() - start_point.getY()) * tested_point.getX() -
			  (end_point.getX() - start_point.getX()) * tested_point.getY()
			  + end_point.getX() * start_point.getY() - end_point.getY() * start_point.getX()
	  );

	  auto denominator = lidar_shared::Point::GetDistanceBetweenPoints(start_point, end_point);

	  if (denominator == 0) {
		  return 0;
	  }

	  return numerator / denominator;
  }
};
}

#endif //LIDAR_SHARED_POINT_H
