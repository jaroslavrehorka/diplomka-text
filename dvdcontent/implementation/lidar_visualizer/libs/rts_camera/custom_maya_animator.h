
#ifndef _CUSTOMMAYAANIMATOR_H_
#define _CUSTOMMAYAANIMATOR_H_

#include <irrlicht.h>

class CustomMayaAnimator : public virtual irr::scene::ISceneNodeAnimator {
  irr::core::vector2di currentMousePos;
  irr::core::vector2di lastMousePos;
  irr::core::vector2di deltaMousePos;
  irr::f32 mouseMotionSensitivity;
  irr::f32 mouseRotationSensitivity;
  irr::f32 cameraFOV;

  bool LMPressed;
  bool RMPressed;
  bool MBPressed;
  irr::f32 wheelMotion;

  irr::u32 currentTime;
  irr::u32 lastTime;
  irr::f32 deltaTime;

public:
  CustomMayaAnimator();

  ~CustomMayaAnimator();

  bool OnEvent(const irr::SEvent &event);

  void animateNode(irr::scene::ISceneNode *node, irr::u32 timeMs);

  irr::scene::ISceneNodeAnimator *createClone(irr::scene::ISceneNode *node, irr::scene::ISceneManager *newManager = 0);
};

#endif
 