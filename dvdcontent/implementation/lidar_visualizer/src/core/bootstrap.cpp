//
// Created by jrehorka on 3/20/17.
//

#include "core/scene_control/scene_control.h"
#include "core/socket_control/socket_control.h"
#include "bootstrap.h"

#include <csignal>
#include <iostream>
#include <cpplinq.hpp>
#include <core/data_filter/data_filter.h>

bool lidar_visualizer::Bootstrap::ctrl_c_pressed = false;

void lidar_visualizer::Bootstrap::StopSignal(int) {
	ctrl_c_pressed = true;
}

lidar_visualizer::Bootstrap::Bootstrap(const lidar_visualizer::Options run_options)
	: run_options(run_options) {}

int lidar_visualizer::Bootstrap::Run() {

	auto return_status = EXIT_SUCCESS;

	auto socket_control = std::make_unique<lidar_visualizer::SocketControl>(run_options);
	auto scene = lidar_visualizer::SceneControl();
	scene.event_receiver.filters = &this->data_filters;

	try {
		socket_control->Connect();

		std::signal(SIGINT, lidar_visualizer::Bootstrap::StopSignal);

		while (scene.IsRunning()) {
			scene.BeginDrawing();
			scene.ClearScene();

			if (socket_control->has_new_data) {
				std::unique_lock<std::mutex> lock(socket_control->data_access_mutex);

				for (auto const point : DataFilter::ApplyFilter(socket_control->points, this->data_filters)) {
					if (!point.IsZeroPoint()) {
						scene.CreatePoint(point.getX(), point.getY(), point.getZ());
					}
				}

				for (auto const line : DataFilter::ApplyFilter(socket_control->lines, data_filters)) {
					scene.CreateLine(line);
				}

				std::wstring info_string =
					L"Points count: " + std::to_wstring(socket_control->points.size()) + L"\nLines detected: "
						+ std::to_wstring(socket_control->lines.size());
				scene.UpdateInfoBox(info_string);
			}

			scene.EndDrawing();

			if (ctrl_c_pressed) {
				break;
			}
		}
	} catch (std::exception &er) {
		std::cerr << er.what() << std::endl;
		return_status = EXIT_FAILURE;
	} catch (libsocket::socket_exception &er) {
		std::cerr << "Socket exception" << er.mesg << std::endl;
		return_status = EXIT_FAILURE;
	} catch (...) {
		std::cerr << "Unknown exception exception" << std::endl;
		return_status = EXIT_FAILURE;
	}

	return return_status;
}
