//
// Created by jrehorka on 3/18/17.
//

#ifndef LIDAR_VISUALIZER_SOCKET_CONTROL_H
#define LIDAR_VISUALIZER_SOCKET_CONTROL_H

#include "shared/model/point.h"
#include "model/options.h"
#include "shared/model/line.h"

#include <string>
#include <vector>
#include <regex>
#include <inetclientstream.hpp>
#include <thread>
#include <mutex>

namespace lidar_visualizer {
/// This class is a simplified wrapper of libsocket library
/// that is used for interprocess communication.
class SocketControl {
private:
  constexpr static char kPointDelimiter = ';';
  constexpr static char kPointAttributesDelimiter = ',';
  constexpr static uint32_t kDataBufferSize = 100000;
  constexpr static int kDefaultDistanceScaling = 10;
  char buffer[kDataBufferSize];
  bool is_running = true;
  std::thread incomming_data_thread;
  lidar_visualizer::Options &options;

  libsocket::inet_stream inet_client;

  lidar_shared::Point ParsePointString(const std::string &point_string);
public:
  bool has_new_data = false;
  std::mutex data_access_mutex;
  std::vector<lidar_shared::Point> points;
  std::vector<lidar_shared::Line> lines;

  ///
  /// Initializes SocketControl with given program options.
  ///
  /// \param server_address address to be used for socket server (eg. "localhost").
  /// \param server_port port to be used for socket server (eg. "1234").
  ///
  SocketControl(lidar_visualizer::Options &options);
  ~SocketControl();

  /// Blocking method that waits for an incoming connection,
  /// block is released when a new connection is estabilished.
  void Connect();

  /// Parses incomming lidar data string.
  ///
  /// Data string has following format:
  ///   quality;distance;angle;
  /// \return Parsed point in instance of lidar_shared::Point
  void ReadData(std::vector<lidar_shared::Point> *points,
                std::vector<lidar_shared::Line> *lines);

  void ReadIncommingData();
};
}
#endif //LIDAR_VISUALIZER_SOCKET_CONTROL_H
