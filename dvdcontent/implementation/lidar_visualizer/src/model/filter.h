//
// Created by jrehorka on 30.4.17.
//

#ifndef LIDAR_VISUALIZER_FILTER_H
#define LIDAR_VISUALIZER_FILTER_H

namespace lidar_visualizer {
/// This structure is a container for a filtering options.
struct Filter {

  /// Flag whether detected lines should be visible in visualizer.
  bool lines_visible = true;

  /// Minimum lenght of displayed lines.
  ///
  /// -1 means that this filter is not active.
  float lines_min_length = -1;

  /// Maximum lenght of displayed lines.
  ///
  /// -1 means that this filter is not active.
  float lines_max_length = -1;

  /// Maximal Root Mean Square Error
  ///
  /// -1 means that this filter is not active.
  float lines_max_rmse = -1;

  /// Flag whether RMSE values should be distinguished by different colors.
  bool lines_rmse_colored = true;

  /// Flag whether detected point should be visible in visualiser.
  bool point_visible = true;

  /// Minimum quality of displayed point.
  ///
  /// -1 means that this filter is not active.
  float points_min_quality = -1;
};
}

#endif //LIDAR_VISUALIZER_FILTER_H
