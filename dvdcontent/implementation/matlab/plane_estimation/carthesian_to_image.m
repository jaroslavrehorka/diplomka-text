function [image] = carthesian_to_image(x,y,z,zoom_factor,max_x,max_y,min_x,min_y,min_z)
    if(length(x) ~= length(y))
        error('x,y,z must have same length');
    end

    if ~exist('max_x', 'var')
        max_x = ceil(max(x)*zoom_factor);
    end
    
    if ~exist('max_y', 'var')
        max_y = ceil(max(y)*zoom_factor);
    end
    
    if ~exist('min_x', 'var')
        min_x = floor(min(x)*zoom_factor);
    end
    
    if ~exist('min_y', 'var')
        min_y = floor(min(y)*zoom_factor);
    end
    
    if ~exist('min_z', 'var')
        min_z = floor(min(z)*zoom_factor);
    end    
    
    img_width = ceil(max_x - min_x) + 1;
    img_heigh = ceil(max_y - min_y) + 1;
    image = newim(img_width, img_heigh);
    total_length = length(x);
    
    for index = 1:total_length
        [img_x,img_y,img_z] = get_image_coordinates(x(index),y(index),z(index),min_x,min_y,min_z,zoom_factor);
        
        image(img_x,img_y) = 1;
    end
end