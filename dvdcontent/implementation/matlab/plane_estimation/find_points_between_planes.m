function [valid_points_indices] = find_points_between_planes(plane1, plane2, point_cloud)

    if plane1(1) ~= plane2(1) || plane1(2) ~= plane2(2) || plane1(3) ~= plane2(3)
        error('planes must be colinear');
    end 
        
    valid_points_indices = [];
    [rows, columns] = size(point_cloud);
    
    lower_d = min(plane1(4), plane2(4));
    upper_d = max(plane1(4), plane2(4));
    
    for column_index = 1 : columns
        point = point_cloud(:,column_index);
        plane_distance = plane1(1)*point(1) + plane1(2)*point(2) + plane1(3)*point(3);
        
        if(plane_distance >= lower_d && plane_distance <= upper_d)
            valid_points_indices = [valid_points_indices; column_index];
        end
    end    
end